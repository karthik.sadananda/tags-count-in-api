const fs = require("fs");
module.exports = async (filename, data) => {
  csv = data.map((row) => Object.values(row));
  csv.unshift(Object.keys(data[0]));
  //console.log(csv);
  fs.writeFile(filename, csv.join("\n"), function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
  return csv.join("\n");
};
