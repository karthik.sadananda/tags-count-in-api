const express = require("express");
const moment = require("moment");
const fetch = require("node-fetch");
const app = express();
const path = require("path");
const _ = require("lodash");
const fileoperations = require("./conversion");
const fs = require("fs");
let startEpoch, endEpoch, accessToken, agent_id, base_url;
let user_count = [],
  user_Arr = [],
  finalcount = [];
let requestOptions = {};
let totalPages;
const per_page = 2000;
let count = 0;
const parallel_count = 6;
let mainArray = ["M200", "M201", "M202", "M203", "M203.1", "M206", "M207"];
let secArray = ["M300", "M307", "M400"];

app.get("/", async function (req, res) {
  const startDate = req.query.start_time;
  console.log(startDate);
  startEpoch = moment(startDate, "DD/MM/YYYY").unix();
  console.log(startEpoch);
  const endDate = req.query.end_time;
  console.log(endDate);
  endEpoch = moment(endDate, "DD/MM/YYYY").unix();
  console.log(endEpoch);
  base_url = req.query.base_url;
  accessToken = req.rawHeaders[1];
  agent_id = req.query.agent_id;
  console.log(base_url);
  console.log(agent_id);
  console.log(accessToken);

  const data = await filterData();
  console.log("the data is sent as response");
  await fileoperations("data.csv", user_count);
  // fs.readFile("data.csv", "utf8", function (err, data) {
  //   //console.log(data);
  //   if (data) {
  //     return res.sendFile("data.csv", { root: path.join(__dirname) });
  //   }
  // });
  res.json(data);
});

const filterData = async () => {
  var myHeaders = {
    "Access-Token": `${accessToken}`,
  };
  requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };
  await fetch(
    `${base_url}/api/v1/agents/${agent_id}/query_insights.json?start_time=${startEpoch}&end_time= ${endEpoch}&per_page=${per_page}`,
    requestOptions
  )
    .then((response) => response.text())
    .then((result) => {
      totalPages = JSON.parse(result).total_pages;
      console.log("totalPages", totalPages);
    })
    .catch((error) => {
      console.log(error);
    });

  for (let i = count + 1; i <= count + parallel_count; i++) {
    if (i <= totalPages) {
      var res = await fetch(
        `${base_url}/api/v1/agents/${agent_id}/query_insights.json?start_time=${startEpoch}}&end_time= ${endEpoch}&per_page=${per_page}&page=${i}`,
        requestOptions
      )
        .then((response) => response.json())
        .then((dataobtained) => {
          processJson(dataobtained);
          return user_count;
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }
  count = count + parallel_count;
  if (count <= totalPages) {
    await filterData();
  } else {
    finalCount(res);
    return finalcount;
    //return res;
  }
};

const renderJson = async (user_Arr) => {
  let counts = 1;
  user_Arr.forEach((user) => {
    counts = 1;
    user.Tags.forEach((tag, index) => {
      // repeat(tag, counts);
      if (
        tag == mainArray[1] ||
        tag == mainArray[2] ||
        tag == mainArray[3] ||
        tag == mainArray[4] ||
        tag == mainArray[5] ||
        tag == mainArray[6]
      ) {
        user_count.push({
          UserId: user.Userid,
          stepsTaken: counts,
          name: tag,
          desp: "successfull",
        });
        counts = 1;
      }
      if (tag == secArray[0] || tag == secArray[1]) {
        user_count.push({
          UserId: user.Userid,
          stepsTaken: counts,
          name: tag,
          desp: "live agent",
        });
        counts = 1;
      }
      if (tag == secArray[2]) {
        user_count.push({
          UserId: user.Userid,
          stepsTaken: counts,
          name: tag,
          desp: "bot execption",
        });
        counts = 1;
      }

      if (tag == null) {
        if (user.Tags.length - 1 == index) {
          user_count.push({
            UserId: user.Userid,
            stepsTaken: counts,
            name: tag,
            desp: "dropoff",
          });
          counts = 1;
        } else {
          counts = counts + 1;
        }
      }
    });
  });
  //console.log(user_count);
  return user_count;
};

const processJson = async (dataobtained) => {
  // function to list all data's of each user
  user_Arr = [];
  count1 = [];
  let unique_users = [];

  dataobtained.queries
    ? dataobtained.queries.forEach((item) => unique_users.push(item.user.id))
    : null;
  unique_users = _.uniq(unique_users);
  unique_users.forEach((userid) => {
    let userQuery = [],
      intentName = [],
      tags = [],
      createdAt = [];
    dataobtained.queries.forEach((qur) => {
      if (userid == qur.user.id) {
        userQuery.push(qur.user_query);
        intentName.push(qur.intent_name);
        createdAt.push(qur.created_at);
        if (!qur?.tags) {
          tags.push(null);
        } else {
          qur?.tags?.forEach((t) => tags.push(t.name));
        }
      }
    });
    if (tags.length > 0) {
      user_Arr.push({
        Userid: userid,
        createdAt: createdAt.reverse(),
        userQuery: userQuery.reverse(),
        intentName: intentName.reverse(),
        Tags: tags.reverse(),
      });
    }
  });
  await renderJson(user_Arr);
  return user_count;
};

const finalCount = async (res) => {
  let s1 = 0,
    s2 = 0,
    s3 = 0,
    s4 = 0,
    s5 = 0,
    s6 = 0,
    s7 = 0;
  let counterA = 0,
    counterB = 0,
    counterC = 0,
    counterD = 0,
    counterE = 0,
    counterF = 0,
    counterG = 0,
    stepsavgA = 0,
    stepsavgB = 0,
    stepsavgC = 0;
  res.forEach((user) => {
    if (user.name == mainArray[1]) {
      counterA = counterA + 1;
      s1 = s1 + user.stepsTaken;
    } else if (user.name == mainArray[2]) {
      counterB = counterB + 1;
      s2 = s2 + user.stepsTaken;
    } else if (user.name == mainArray[3] || user.name == mainArray[4]) {
      counterC = counterC + 1;
      s3 = s3 + user.stepsTaken;
    } else if (user.name == mainArray[5] || user.name == mainArray[6]) {
      counterD = counterD + 1;
      s4 = s4 + user.stepsTaken;
    } else if (user.desp == "live agent") {
      counterE = counterE + 1;
      s5 = s5 + user.stepsTaken;
    } else if (user.desp == "bot execption") {
      counterF = counterF + 1;
      s6 = s6 + user.stepsTaken;
    } else if (user.desp == "dropoff") {
      counterG = counterG + 1;
      s7 = s7 + user.stepsTaken;
    }
  });
  stepsavgB = s2 / counterB;
  stepsavgC = s3 / counterC;
  stepsavgA = s1 / counterA;
  let sABC = s1 + s2 + s3 + s4;
  let countersuccess = counterA + counterB + counterC + counterD;
  console.log(countersuccess, counterE, counterF, counterG);
  console.log(sABC, s5, s6, s7);

  finalcount.push({
    "Successfully given part availability details  M201": stepsavgA.toFixed(0),
    "Successfully given part pricing details  M202": stepsavgB.toFixed(0),
    "Successfully given order status  M203": stepsavgC.toFixed(0),
    "successfull tags M200": countersuccess,
    "live agent M300": counterE,
    "bot exception M400": counterF,
    "dropoff tags": counterG,
  });
  return finalcount;
};

app.listen(3029, () => {
  console.log("Started on PORT 3029");
});
